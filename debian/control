Source: stardict
Section: utils
Priority: optional
Maintainer: xiao sheng wen <atzlinux@sina.com>
Build-Depends:
 debhelper-compat (= 13),
 gnome-common,
 libgconf2-dev,
 libgucharmap-2-90-dev,
 flite1-dev,
 libespeak-ng-dev,
 libcanberra-gtk3-dev,
 docbook-xml,
 intltool,
 libenchant-2-dev | libenchant-dev,
 libespeak-dev,
 libestools-dev,
 libgtk-3-dev,
 default-libmysqlclient-dev,
 libpcre3-dev,
 libsigc++-2.0-dev (>= 2.0.18-2),
 libtool,
 libx11-dev,
 libxml2-dev,
 libxml-parser-perl,
 festival-dev,
 sharutils,
 x11proto-core-dev,
 xauth <!nocheck>,
 xvfb <!nocheck>,
 zlib1g-dev,
Build-Conflicts: autoconf2.13, automake1.4
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: http://stardict-4.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/stardict.git
Vcs-Browser: https://salsa.debian.org/debian/stardict

Package: stardict
Architecture: all
Depends:
 ${misc:Depends}, ${shlibs:Depends},
 stardict-gtk (>= ${source:Version})
Description: International dictionary lookup program - stardict.png icons
 StarDict is a cross-platform international dictionary lookup program.
 .
 Main features:
  * Glob-style pattern matching search
  * Fuzzy search
  * Working from system tray
  * Scanning mouse selection and showing pop-up windows with translation of
    selected words
  * Pronouncing of the translated words
  * Plugins support
  * ..and more
 .
 This package contains different size stardict.png icons files.

Package: stardict-gtk
Architecture: any
Depends:
 ${misc:Depends}, ${shlibs:Depends}, stardict-common (= ${source:Version})
Recommends:
 stardict-plugin (= ${binary:Version}),
 stardict-plugin-espeak (= ${binary:Version}),
 stardict-plugin-festival (= ${binary:Version})
Suggests: fonts-freefont-ttf, fonts-arphic-uming
Breaks: stardict-common (<< 3.0.6)
Replaces: stardict-gnome, stardict-common (<< 3.0.6)
Conflicts: stardict-gnome
Provides: stardict
Description: International dictionary lookup program - gtk
 StarDict is a cross-platform international dictionary lookup program.
 .
 Main features:
  * Glob-style pattern matching search
  * Fuzzy search
  * Working from system tray
  * Scanning mouse selection and showing pop-up windows with translation of
    selected words
  * Pronouncing of the translated words
  * Plugins support
  * ..and more
 .
 This package is compiled without GNOME support and contains only
 the program of stardict and will be of limited use without the
 dictionary files. For more information about how to get the
 dictionary files, please read /usr/share/doc/stardict/README.Debian.

Package: stardict-plugin-espeak
Architecture: any
Depends:
 ${misc:Depends}, ${shlibs:Depends}, stardict-common (= ${source:Version}),
 stardict-gtk (= ${binary:Version}),
 stardict-plugin (= ${binary:Version})
Description: International dictionary lookup program - eSpeak TTS plugin
 StarDict is a cross-platform international dictionary lookup program.
 .
 Main features:
  * Glob-style pattern matching search
  * Fuzzy search
  * Working from system tray
  * Scanning mouse selection and showing pop-up windows with translation of
    selected words
  * Pronouncing of the translated words
  * Plugins support
  * ..and more
 .
 This package contains eSpeak TTS plugin for StarDict which can pronounce
 words.

Package: stardict-plugin-festival
Architecture: any
Depends:
 ${misc:Depends}, ${shlibs:Depends}, stardict-common (= ${source:Version}),
 stardict-gtk (= ${binary:Version}),
 stardict-plugin (= ${binary:Version})
Description: International dictionary lookup program - Festival TTS plugin
 StarDict is a cross-platform international dictionary lookup program.
 .
 Main features:
  * Glob-style pattern matching search
  * Fuzzy search
  * Working from system tray
  * Scanning mouse selection and showing pop-up windows with translation of
    selected words
  * Pronouncing of the translated words
  * Plugins support
  * ..and more
 .
 This package contains Festival TTS plugin for StarDict which can pronounce
 words.

Package: stardict-plugin-spell
Architecture: any
Depends:
 ${misc:Depends}, ${shlibs:Depends}, stardict-common (= ${source:Version}),
 stardict-gtk (= ${binary:Version}),
 stardict-plugin (= ${binary:Version})
Description: International dictionary lookup program - spell plugin
 StarDict is a cross-platform international dictionary lookup program.
 .
 Main features:
  * Glob-style pattern matching search
  * Fuzzy search
  * Working from system tray
  * Scanning mouse selection and showing pop-up windows with translation of
    selected words
  * Pronouncing of the translated words
  * Plugins support
  * ..and more
 .
 This package contains spell plugin for StarDict which give you spelling
 suggestion while you searching the dictionary.

Package: stardict-plugin
Architecture: any
Depends:
 ${misc:Depends}, ${shlibs:Depends}, stardict-common (= ${source:Version}),
 stardict-gtk (= ${binary:Version})
Description: International dictionary lookup program - common plugins
 StarDict is a cross-platform international dictionary lookup program.
 .
 Main features:
  * Glob-style pattern matching search
  * Fuzzy search
  * Working from system tray
  * Scanning mouse selection and showing pop-up windows with translation of
    selected words
  * Pronouncing of the translated words
  * Plugins support
  * ..and more
 .
 This package contains some add-on plugins for StarDict. Includes:
  - man virtual dict plugin
  - qqwry virtual dict plugin
  - html parse plugin
  - xdxf parse plugin
  - wordnet plugin
  - powerword parse plugin
  - wiki parse plugin
  - customdict plugin
  - stardict_cal plugin
  - stardict_espeak_ng plugin
  - stardict_flite plugin
  - stardict_fortune plugin
  - stardict_gucharmap plugin
  - stardict_info plugin
  - stardict_update_info plugin
  - stardict_youdaodict plugin

Package: stardict-common
Architecture: all
Depends: ${misc:Depends}, sgml-data (>= 2.0.2)
Recommends: stardict-gtk (>= ${source:Version})
Breaks: stardict-gnome (= 3.0.1-8), stardict-gtk (= 3.0.1-8)
Replaces: stardict-gnome (= 3.0.1-8), stardict-gtk (= 3.0.1-8)
Description: International dictionary lookup program - data files
 StarDict is a cross-platform international dictionary lookup program.
 .
 Main features:
  * Glob-style pattern matching search
  * Fuzzy search
  * Working from system tray
  * Scanning mouse selection and showing pop-up windows with translation of
    selected words
  * Pronouncing of the translated words
  * Plugins support
  * ..and more
 .
 This package contains data files (pixmaps, icons, documentations) needed
 by the stardict.

Package: stardict-tools
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, python3, dictzip
Suggests: babiloo | goldendict | qstardict | stardict | stardict-gtk
Description: dictionary conversion tools of stardict
 StarDict is a cross-platform international dictionary lookup program.
 .
 Main features:
  * Glob-style pattern matching search
  * Fuzzy search
  * Working from system tray
  * Scanning mouse selection and showing pop-up windows with translation of
    selected words
  * Pronouncing of the translated words
  * Plugins support
  * ..and more
 .
 This package contains the dictionary conversion tools which can convert
 dictionaries of DICT, wquick, mova and pydict to stardict format.
